# Nlp-Sentiment-Analysis
 Le présent projet vise à mettre en œuvre un système de classification de tweets capable d’attribuer à chaque message l’une des classes représentant différents niveaux d’intensité positive et négative du sentiment avec l’utilisation de réseaux de neurones récurrents  LSTM.
